import { useEffect } from 'react';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { api } from '../api';
import { IProfile } from '../types';
import { authActions } from '../lib/redux/actions';

export const useSingUp = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const mutation = useMutation((user: IProfile) => {
        return api.users.create(user);
    });

    useEffect(() => {
        if (mutation.isSuccess) {
            const token = mutation.data;
            dispatch(authActions.setToken(token));
            localStorage.setItem('token', token);
            navigate('/');
        }
    }, [mutation.isSuccess]);

    return mutation;
};
