import { useMutation } from 'react-query';
import { api } from '../api';

export const useRemoveAllRecords = () => {
    const token = localStorage.getItem('token');

    const mutation = useMutation(() => {
        return api.tracker.removeAllRecords(token);
    });

    return mutation;
};
