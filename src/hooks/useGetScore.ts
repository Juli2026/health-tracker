import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { api } from '../api';

export const useGetStore = () => {
    const token = localStorage.getItem('token');
    const MAX_SCORE = 100;
    const query = useQuery(
        ['score', token],
        () => api.tracker.getScore(token),
    );

    const { data } = query;

    const checkScore = data as number > MAX_SCORE ? MAX_SCORE : data;

    return checkScore;
};
