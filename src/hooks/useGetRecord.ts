import { useQuery, UseQueryResult } from 'react-query';
import { api } from '../api';
import { IResponseRecord } from '../types';

export const useGetRecord = (kind: string): UseQueryResult<IResponseRecord> => {
    const token = localStorage.getItem('token');

    const query = useQuery<IResponseRecord>(['record', token],
        () => api.tracker.getRecord(kind, token));

    return (
        query
    );
};
