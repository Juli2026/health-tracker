import { useMutation } from 'react-query';
import { api } from '../api';
import { queryClient } from '../lib/queryClient';
import { IRecord } from '../types';

export const useCreateRecord = () => {
    const token = localStorage.getItem('token');

    const mutation = useMutation((newRecord: IRecord) => {
        return api.tracker.createRecord(newRecord, token);
    },
    {
        onSuccess: () => queryClient.invalidateQueries('score'),
    });

    return mutation;
};
