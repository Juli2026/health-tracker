import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from 'react-query';
import { IProfile } from '../types';
import { getIsLoading, getProfile } from '../lib/redux/selectors';
import { profileActions } from '../lib/redux/actions';
import { api } from '../api';

export const useGetProfile = () => {
    const dispatch = useDispatch();
    const token = localStorage.getItem('token');

    const user = useSelector(getProfile);
    const isLoading = useSelector(getIsLoading);

    const query = useQuery<IProfile>(['profile', token],
        () => api.users.getMe(token));
    const { data, refetch } = query;

    const getSex = data?.sex;
    const fetchUserProfile = () => dispatch(profileActions.fetchProfileAsync(token as string));
    const setUserProfile = (value: IProfile | null) => dispatch(profileActions.setProfile(value));


    return {
        user,
        refetch,
        getSex,
        data,
        isLoading,
        fetchUserProfile,
        setUserProfile,
    };
};

