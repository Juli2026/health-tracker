import { useEffect } from 'react';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { api } from '../api';
import { profileActions } from '../lib/redux/actions';
import { IProfile } from '../types';

export const useUpdateProfile = () => {
    const dispatch = useDispatch();
    const token = localStorage.getItem('token');
    const mutation = useMutation((payload: IProfile) => {
        return api.users.updateMe(payload, token);
    });

    useEffect(() => {
        if (mutation.isSuccess) {
            const { ...newUser } = mutation.data;
            dispatch(profileActions.setProfile(newUser));
        }
    }, [mutation.isSuccess]);

    return mutation;
};
