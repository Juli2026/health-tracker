import { useNavigate } from 'react-router-dom';
import { useGetProfile } from './useProfile';

export const useLogout = () => {
    const navigate = useNavigate();

    const { setUserProfile } = useGetProfile();

    const token = localStorage.getItem('token');

    const setToken = (jwt: string) => {
        localStorage.setItem('token', jwt);
    };

    const removeToken = () => {
        setUserProfile(null);
        navigate('/login');
        localStorage.removeItem('token');
    };

    return {
        token,
        setToken,
        removeToken,
    };
};

