import { useMutation } from 'react-query';
import { api } from '../api';
import { IRecord } from '../types';

type VariablesType = {
    record: IRecord,
    hash: string,
};


export const useUpdateRecord = () => {
    const token = localStorage.getItem('token');

    const mutation
        = useMutation(({ record, hash }: VariablesType) => {
            return api.tracker.updateRecord(record, hash, token);
        });


    return mutation;
};
