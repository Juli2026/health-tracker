import { Link } from 'react-router-dom';
import { useLogout } from '../../../hooks/useLogout';
import { useGetProfile } from '../../../hooks/useProfile';
import Styled from '../styles/index.module.scss';

export const ProfileBase = () => {
    const { data, getSex } = useGetProfile();
    const { removeToken } = useLogout();
    const logout = () => removeToken();

    const iconPicture
    = getSex === 'm' ? <div className = { Styled.user_male } /> :  <div className = { Styled.user_female } />;


    return (
        <div className = { `${Styled.user_avatar}` } style = { { justifyContent: 'end', margin: '30px' } }>
            <div className = { Styled.user_column } >
                <Link to = '/profile' className = { Styled.user_name }>
                    { data?.fname } { data?.lname }
                </Link>
                <button
                    className = { Styled.user_logout }
                    onClick = { logout }>Выйти</button>
            </div>
            { iconPicture }
        </div>
    );
};
