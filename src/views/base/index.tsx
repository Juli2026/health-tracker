/* eslint-disable max-len */
// Core
import { FC, ReactElement } from 'react';
import cx from 'classnames';
import { observer } from 'mobx-react-lite';

// Book
import { book } from '../../navigation/book';

// Styles
import Styles from './styles/index.module.scss';

// Elements
import { Spinner } from '../../elements/spinner';

// Hooks
import { useStore } from '../../hooks';
import { useGetProfile } from '../../hooks/useProfile';
import { Sex } from '../../types';
import { ProfileBase } from './Profile/profileBase';
import { useGetStore } from '../../hooks/useGetScore';


export const Base: FC<IPropTypes> = observer((props) => {
    const { uiStore } = useStore();
    const { isLoading } = uiStore;

    const { getSex } = useGetProfile();

    const {
        children,
        center,
        disabledWidget,
    } = props;


    const score = useGetStore();

    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   getSex === Sex.MALE,
            [ Styles.female ]: getSex === Sex.FEMALE,
        },
    ]);

    const contentCX = cx(Styles.content, {
        [ Styles.center ]: center,
    });

    const loaderCX = isLoading && (
        <Spinner isLoading = { isLoading } />
    );


    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { Styles.widget }>
            <span className = { Styles.title }>Life Score</span>
            <div className = { Styles.module }>
                <span className = { Styles.score } style = { { bottom: `${score}%` } }>{ score }</span>
                <div className = { Styles.progress }>
                    <div className = { Styles.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx([Styles.label, Styles.level1]) }>Off Track</span>
                <span className = { cx([Styles.label, Styles.level2]) }>Imbalanced</span>
                <span className = { cx([Styles.label, Styles.level3]) }>Balanced</span>
                <span className = { cx([Styles.label, Styles.level4]) }>Healthy</span>
                <span className = { cx([Styles.label, Styles.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );

    return (
        <section>
            <div className = { Styles.item }>
                <div className = { avatarCX }>
                    { loaderCX }
                </div>
                <div className = { Styles.wrap }>
                    <ProfileBase />

                    <div className = { contentCX }>
                        { children }
                        { widgetJSX }
                    </div>
                </div>
            </div>

        </section>
    );
});

interface IPropTypes {
    children: ReactElement | ReactElement[];
    center?: boolean;
    disabledWidget?: boolean;
}
