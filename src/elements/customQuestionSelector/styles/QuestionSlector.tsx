import { FC, useEffect, useState } from 'react';
import cx from 'classnames';

import { observer } from 'mobx-react-lite';
import { ValueType } from '../../../types';
// import { useGetRecord, useCreateRecord, useUpdateRecord } from 'hooks';

import styles from './index.module.scss';
import { useGetRecord } from '../../../hooks/useGetRecord';
import { useCreateRecord } from '../../../hooks/useCreateRecord';
import { useUpdateRecord } from '../../../hooks/useUpdateRecord';

interface ISelectorType  {
    id:    number;
    label: string;
    value: string | boolean;
}

interface IQuestionSlectorProps {
    type: string;
    title: string;
    inputs: ISelectorType[];
}

export const QuestionSlector: FC<IQuestionSlectorProps> = observer(({ type, title, inputs }) => {
    const [value, setValue] = useState<ValueType | undefined>();
    const { data, isFetched } = useGetRecord(type);
    const createRecord = useCreateRecord();
    const updateRecord = useUpdateRecord();

    useEffect(() => {
        if (isFetched && data) {
            setValue(data.value);
        }
    }, [isFetched, data]);

    const handleSubmit = async () => {
        if (value === undefined) {
            return;
        }

        const record = {
            type,
            record: value,
        };

        if (data?.hash && data?.hash !== '0') {
            await updateRecord.mutateAsync({ record, hash: data.hash });
        } else {
            await createRecord.mutateAsync(record);
        }
    };

    return (
        <div className = { styles.question }>
            <h1>{ title }</h1>
            <div className = { styles.answers }>
                { inputs.map(({ id, value: inputValue, label }) => (
                    <span
                        key = { id }
                        className = {  cx(styles.answer, {
                            [ styles.selected ]: value === inputValue,
                        })  }
                        onClick = { () => setValue(inputValue) }>{ label }</span>
                )) }
            </div>

            <button
                className = { styles.sendAnswer }
                disabled = { value === undefined }
                onClick = { handleSubmit }>
                    Ответить
            </button>
        </div>
    );
});
