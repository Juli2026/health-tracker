import { UseFormRegisterReturn } from 'react-hook-form';

export const Input: React.FC<IPropTypes> = (props) => {
    const input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            value = { props.value }
            { ...props.register } />
    );

    return (
        <label>
            <div>
                <span className = 'error'>{ props.error?.message }</span>
            </div>
            { input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};

interface IPropTypes {
    placeholder?: string;
    type?: string;
    value?: string | number;
    tag?: string;
    disabled?: boolean;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
}
