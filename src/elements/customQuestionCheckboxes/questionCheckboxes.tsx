import { FC, useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import cx from 'classnames';

import { ValueType } from '../../types';
import { useGetRecord } from '../../hooks/useGetRecord';
import { useCreateRecord } from '../../hooks/useCreateRecord';
import { useUpdateRecord } from '../../hooks/useUpdateRecord';

import styles from './styles/index.module.scss';

interface ISelectorType  {
    id:    number;
    value: number;
}

interface IQuestionCheckboxesProps {
    type: string;
    title: string;
    inputs: ISelectorType[];
    multiplier?: number;
}

export const QuestionCheckboxes: FC<IQuestionCheckboxesProps>
= observer(({
    type, title, inputs, multiplier = 1,
}) => {
    const [value, setValue] = useState<ValueType>(0);
    const { data, isFetched } = useGetRecord(type);
    const createRecord = useCreateRecord();
    const updateRecord = useUpdateRecord();

    useEffect(() => {
        if (isFetched && data?.value) {
            setValue(data.value);
        }
    }, [isFetched, data]);

    const handleSubmit = async () => {
        const record = {
            type,
            record: value,
        };

        if (data?.hash && data?.hash !== '0') {
            await updateRecord.mutateAsync({ record, hash: data.hash });
        } else {
            await createRecord.mutateAsync(record);
        }
    };

    return (
        <div className = { styles.question }>
            <h1>{ title }</h1>
            <div className = { styles.elements }>
                { inputs.map(({ id, value: inputValue }) => (
                    <button
                        key = { id }
                        className = { cx(styles.element, {
                            [ styles.selected ]: inputValue <= value,
                        })  }
                        onClick = { () => setValue(inputValue) }></button>
                )) }
                <span className = { styles.size }>{ multiplier * Number(value) } мл</span>
            </div>

            <button
                disabled = { value === 0 }
                className = { styles.sendAnswer }
                onClick = { handleSubmit }>
                    Ответить
            </button>
        </div>
    );
});
