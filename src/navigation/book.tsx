// Components
import { Login } from '../bus/user/components/login/login';
import { Profile } from '../bus/user/components/profile/profile';
import { SingUp } from '../bus/user/components/registration/singUp';
import { Dashboard } from '../pages';
import { BreakFast } from '../pages/breakfast/breakFast';
import { Coffee } from '../pages/coffee/coffee';
import { Dinner } from '../pages/dinner/dinner';
import { FastFood } from '../pages/fastfood/fastfood';
import { Fruits } from '../pages/fruits/fruits';
import { Lunch } from '../pages/lunch/lunch';
import { Sleeps } from '../pages/sleeps/sleeps';
import { Steps } from '../pages/steps/steps';
import { Vegetables } from '../pages/vegetables/vegetables';
import { Water } from '../pages/water/water';

const base = '/';

export const book = Object.freeze({
    root: {
        url:  `${base}`,
        page: Dashboard,
    },
    login: {
        url:  `${base}login`,
        page: () => <Login />,
    },
    registration: {
        url:  `${base}registration`,
        page: () => <SingUp />,
    },
    profile: {
        url:  `${base}profile`,
        page: () => <Profile />,
    },
    breakfast: {
        url:  `${base}breakfast`,
        page: () => <BreakFast />,
    },
    coffee: {
        url:  `${base}coffee`,
        page: () => <Coffee />,
    },
    dinner: {
        url:  `${base}dinner`,
        page: () => <Dinner />,
    },
    fruits: {
        url:  `${base}fruits`,
        page: () => <Fruits />,
    },
    junk: {
        url:  `${base}junk`,
        page: () => <FastFood />,
    },
    lunch: {
        url:  `${base}lunch`,
        page: () => <Lunch />,
    },
    sleep: {
        url:  `${base}sleep`,
        page: () => <Sleeps />,
    },
    steps: {
        url:  `${base}steps`,
        page: () => <Steps />,
    },
    vegetables: {
        url:  `${base}vegetables`,
        page: () => <Vegetables />,
    },
    water: {
        url:  `${base}water`,
        page: () => <Water />,
    },
});

export type BookType = typeof book;
