// Core
import { FC } from 'react';

// Views
import { observer } from 'mobx-react-lite';
import { Base } from '../../views/base';
import { DashboardItems } from '../../bus/tracker/components/dashboard/dashboardItems';

export const Dashboard: FC = observer(() => {
    return (
        <>
            <Base>
                <DashboardItems />
            </Base>
        </>
    );
});
