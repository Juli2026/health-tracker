import cx from 'classnames';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import { QuestionCheckboxes } from '../../elements/customQuestionCheckboxes/questionCheckboxes';
import { useGetStore } from '../../hooks/useGetScore';
import { useGetProfile } from '../../hooks/useProfile';
import { Sex } from '../../types';
import { ProfileBase } from '../../views/base/Profile/profileBase';
import Styles from '../../views/base/styles/index.module.scss';

const water = {
    type:       'water',
    title:      'Сколько воды ты сегодня выпил?',
    inputs:     [...Array(12).keys()].map((i) => ({ id: i + 1, value: i + 1 })),
    multiplier: 250,
};

export const Water: FC<IPropTypes> = (props) => {
    const { getSex } = useGetProfile();

    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   getSex === Sex.MALE,
            [ Styles.female ]: getSex === Sex.FEMALE,
        },
    ]);

    const homeLinkJSX = (
        <Link to = '/' className = { Styles.homeLink }>На главную</Link>
    );

    const { disabledWidget } = props;
    const score = useGetStore();

    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { Styles.widget }>
            <span className = { Styles.title }>Life Score</span>
            <div className = { Styles.module }>
                <span className = { Styles.score } style = { { bottom: `${score}%` } }>{ score }</span>
                <div className = { Styles.progress }>
                    <div className = { Styles.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx([Styles.label, Styles.level1]) }>Off Track</span>
                <span className = { cx([Styles.label, Styles.level2]) }>Imbalanced</span>
                <span className = { cx([Styles.label, Styles.level3]) }>Balanced</span>
                <span className = { cx([Styles.label, Styles.level4]) }>Healthy</span>
                <span className = { cx([Styles.label, Styles.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );

    return (
        <section>
            <div className = { Styles.item }>
                <div className = { avatarCX  }></div>
                <div className = { Styles.wrap }>
                    <div className = { Styles.profile } style = { { justifyContent: 'space-between' } }>
                        { homeLinkJSX }
                        <ProfileBase />
                    </div>
                    <div className = { Styles.content }>
                        <div className = { Styles.question }>
                            <QuestionCheckboxes { ...water } />
                        </div>
                        { widgetJSX }
                    </div>
                </div>
            </div>
        </section>
    );
};


interface IPropTypes {
    disabledWidget?: boolean;
}
