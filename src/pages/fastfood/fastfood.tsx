import cx from 'classnames';
import { Link } from 'react-router-dom';
import { QuestionSlector } from '../../elements/customQuestionSelector/styles/QuestionSlector';
import { useGetProfile } from '../../hooks/useProfile';
import { Sex } from '../../types';
import Styles from '../../views/base/styles/index.module.scss';
import { useGetStore } from '../../hooks/useGetScore';
import { ProfileBase } from '../../views/base/Profile/profileBase';

const fastfood = {
    type:   'junk',
    title:  'Ты сегодня кушал Фастфуд?',
    inputs: [
        {
            id:    1,
            label: 'Да',
            value: true,
        },
        {
            id:    2,
            label: 'Нет',
            value: false,
        },
    ],
};


export const FastFood: React.FC<IPropTypes> = (props) => {
    const { getSex } = useGetProfile();

    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   getSex === Sex.MALE,
            [ Styles.female ]: getSex === Sex.FEMALE,
        },
    ]);

    const homeLinkJSX = (
        <Link to = '/' className = { Styles.homeLink }>На главную</Link>
    );

    const { disabledWidget } = props;
    const score = useGetStore();

    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { Styles.widget }>
            <span className = { Styles.title }>Life Score</span>
            <div className = { Styles.module }>
                <span className = { Styles.score } style = { { bottom: `${score}%` } }>{ score }</span>
                <div className = { Styles.progress }>
                    <div className = { Styles.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx([Styles.label, Styles.level1]) }>Off Track</span>
                <span className = { cx([Styles.label, Styles.level2]) }>Imbalanced</span>
                <span className = { cx([Styles.label, Styles.level3]) }>Balanced</span>
                <span className = { cx([Styles.label, Styles.level4]) }>Healthy</span>
                <span className = { cx([Styles.label, Styles.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );

    return (
        <section>
            <div className = { Styles.item }>
                <div className = { avatarCX  }></div>
                <div className = { Styles.wrap }>
                    <div className = { Styles.profile } style = { { justifyContent: 'space-between' } }>
                        { homeLinkJSX }
                        <ProfileBase />
                    </div>
                    <div className = { Styles.content }>
                        <div className = { Styles.question }>
                            <QuestionSlector { ...fastfood } />
                        </div>
                        { widgetJSX }
                    </div>
                </div>
            </div>
        </section>
    );
};

interface IPropTypes {
    disabledWidget?: boolean;
}
