// Core
import axios, { AxiosResponse } from 'axios';

// Config
import { root } from './config';

// Types
import { IProfile } from '../types';
import { ILogin } from '../types/auth';
import { ILoginFormShape } from '../bus/user/components/types';

export const users = {
    getMe: async (token: string | null): Promise<IProfile> => {
        const { data } = await axios.get<IProfile>(`${root}/profile`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },

    create: async (payload: IProfile): Promise<string> => {
        const { data } = await axios.post<AxiosResponse<string>>(`${root}/users`, payload);

        return data.data;
    },
    updateMe: async (payload: IProfile, token: string | null): Promise<IProfile> => {
        const { data } = await axios.put<AxiosResponse<IProfile>>(`${root}/users`, payload, {
            headers: {
                authorization: `Bearer ${token}`,
            },

        });

        return data.data;
    },
    login: async (credentials: ILoginFormShape): Promise<ILogin> => {
        const { email, password } = credentials;
        const { data } = await axios.get<ILogin>(`${root}/login`,
            {
                headers: {
                    Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
                },
            });

        return data;
    },
    logout: async (token: string | null): Promise<void> => {
        if (!token) {
            throw new Error('токен не указан');
        }

        await axios.get(`${root}/logout`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
    },
};
