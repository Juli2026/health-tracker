import { Dispatch } from 'redux';
import { IProfile } from '../../../types';
import { api } from '../../../api';
import { authActions } from './auth';
import { profileTypes } from '../types';

export const profileActions = Object.freeze({
    setProfile: (user: IProfile | null) => {
        return {
            type:    profileTypes.SET_PROFILE,
            payload: user,
        };
    },

    setIsLoading: (value: boolean) => {
        return {
            type:    profileTypes.IS_LOADING,
            payload: value,
        };
    },


    fetchProfileAsync: (token: string) => async (dispatch: Dispatch) => {
        try {
            dispatch(profileActions.setIsLoading(true));


            const profile = await api.users.getMe(token);

            dispatch(profileActions.setProfile(profile));
        } catch (error) {
            const { message } = error as Error;
            dispatch(authActions.setError(message));
        } finally {
            dispatch(profileActions.setIsLoading(false));
        }
    },
});

