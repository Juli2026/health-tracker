export const    profileTypes  = Object.freeze({
    SET_PROFILE: 'SET_PROFILE',
    IS_LOADING:  'IS_LOADING',
});
