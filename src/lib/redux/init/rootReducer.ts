// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth,
    profileReducer as profile,
} from '../reducers';


export const rootReducer = combineReducers({
    auth,
    profile,
});
