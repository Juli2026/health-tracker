export const navItems = [
    {
        index: '0',
        href:  '/breakfast',
        title: 'Добавить завтрак',
        text:  'Хороший завтрак очень важен',
    },
    {
        index: '1',
        href:  '/lunch',
        title: 'Добавить обед',
        text:  'Успешные люди обедают',
    },
    {
        index: '2',
        href:  '/dinner',
        title: 'Добавить ужин',
        text:  'Лучше не ужинать вообще',
    },
    {
        index: '3',
        href:  '/steps',
        title: 'Добавить активность',
        text:  'Пешие прогулки это минимум',
    },
    {
        index: '4',
        href:  '/fruits',
        title: 'Добавить фрукты',
        text:  'Фрукты подымают настроение',
    },
    {
        index: '5',
        href:  '/vegetables',
        title: 'Добавить овощи',
        text:  'Овощи очень важны',
    },
    {
        index: '6',
        href:  '/junk',
        title: 'Добавить фастфуд',
        text:  'Эта еда очень вредная',
    },
    {
        index: '7',
        href:  '/water',
        title: 'Добавить воду',
        text:  'Вода это жизнь',
    },
    {
        index: '8',
        href:  '/sleep',
        title: 'Добавить сон',
        text:  'Спать нужно всем',
    },
    {
        index: '9',
        href:  '/coffee',
        title: 'Добавить кофе',
        text:  'Можно и без него',
    },
];
