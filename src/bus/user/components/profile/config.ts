import * as yup from 'yup';
import {
    tooShortMessage,
    tooLongMessage,
    wrongEmailMessage,
    requiredEmail,
    requiredSex,
    shouldBeNumber,
    tooSmallValue,
    PASSWORD_MIN_LENGTH,
    PASSWORD_MAX_LENGTH,
    TEXT_MIN_LENGTH,
    TEXT_MAX_LENGTH,
} from '../../../../constants/forms';

export interface IProfileFormShape {
    fname?: string;
    lname?: string;
    email: string;
    password: string;
    age?: number;
    sex: string,
    height?: number;
    weight: number;
}

export const profileSchema: yup.SchemaOf<IProfileFormShape> = yup.object().shape({
    email: yup
        .string()
        .email(wrongEmailMessage)
        .required(requiredEmail),
    fname: yup
        .string()
        .min(TEXT_MIN_LENGTH, tooShortMessage)
        .max(TEXT_MAX_LENGTH, tooLongMessage),
    lname: yup
        .string()
        .min(TEXT_MIN_LENGTH, tooShortMessage)
        .max(TEXT_MAX_LENGTH, tooLongMessage),
    password: yup
        .string()
        .min(PASSWORD_MIN_LENGTH, tooShortMessage)
        .max(PASSWORD_MAX_LENGTH, tooLongMessage)
        .required('*'),
    age: yup
        .number()
        .typeError(shouldBeNumber)
        .required()
        .min(18, tooSmallValue),
    sex: yup
        .string()
        .required(requiredSex),
    height: yup
        .number()
        .typeError(shouldBeNumber),
    weight: yup
        .number()
        .typeError(shouldBeNumber)
        .required()
        .min(50, tooSmallValue),
});
