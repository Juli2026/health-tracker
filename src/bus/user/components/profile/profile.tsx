import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { useGetProfile } from '../../../../hooks/useProfile';
import { IProfile, Sex } from '../../../../types';
import Styles from './styles/index.module.scss';
import { profileSchema } from './config';
import { Input } from '../../../../elements/customInput/input';
import { useUpdateProfile } from '../../../../hooks/useUpdateMe';
import { useRemoveAllRecords } from '../../../../hooks/useRemoveAllRecords';
import { ProfileTop } from './ProfileTop';

export const Profile: React.FC = () => {
    const { data, getSex } = useGetProfile();
    const [sexInForm, setSexInForm] = useState('');
    const updateProfile = useUpdateProfile();
    const remove = useRemoveAllRecords();

    const form = useForm({
        mode:          'onTouched',
        defaultValues: {
            email:    data?.email,
            fname:    data?.fname,
            lname:    data?.lname,
            age:      data?.age,
            weight:   data?.weight,
            sex:      data?.sex,
            height:   data?.height,
            password: '',
        },
        resolver: yupResolver(profileSchema),
    });


    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   getSex === Sex.MALE,
            [ Styles.female ]: getSex === Sex.FEMALE,
        },
    ]);

    const handleGender = (gender: Sex) => {
        form.setValue('sex', gender, { shouldDirty: true });
        setSexInForm(gender);
    };

    const handleClick = () => {
        void remove.mutateAsync();
    };

    const iconProfile
    = getSex === 'm'
        ?            <div
            className = { ` ${Styles.male} ${Styles.selected} ` }
            onClick = { () => handleGender(Sex.MALE) }><span>Мужчина</span></div>

        :  <div
            className = { ` ${Styles.female} ${Styles.selected} ` }
            onClick = { () => handleGender(Sex.FEMALE) }><span>Женщина</span></div>;


    const handleReset = () => {
        form.reset();
    };

    const submit = form.handleSubmit(async (values: IProfile) => {
        await updateProfile.mutateAsync(values);
    });

    return (
        <section>
            <div className = { Styles.item }>
                <div className = { avatarCX  }></div>
                <div className = { Styles.wrap }>
                    <ProfileTop />
                    <div className = { Styles.content }>
                        <div className = { Styles.box }>
                            <h1>Профиль</h1>
                            <div className = { Styles.gender }>
                                { iconProfile }
                            </div>
                            <form
                                onSubmit = { submit }>
                                <div className = { Styles.inputRow }>
                                    <label>Электропочта</label>
                                    <Input
                                        type = 'email'
                                        placeholder = 'Введите свою электропочту'
                                        register = { form.register('email') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Имя</label>
                                    <Input
                                        type = 'text'
                                        placeholder = 'Введите свое имя'
                                        register = { form.register('fname') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Фамилия</label>
                                    <Input
                                        type = 'text'
                                        placeholder = 'Введите свою фамилию'
                                        register = { form.register('lname') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Пароль</label>
                                    <Input
                                        type = 'password'
                                        placeholder = 'Введите свой пароль'
                                        register = { form.register('password') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Возраст</label>
                                    <Input
                                        type = 'number'
                                        placeholder = 'Введите свой возрост'
                                        register = { form.register('age') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Рост</label>
                                    <Input
                                        type = ''
                                        placeholder = 'Введите свой рост'
                                        register = { form.register('height') } />
                                </div>
                                <div className = { Styles.inputRow }>
                                    <label>Вес</label>
                                    <Input
                                        type = ''
                                        placeholder = 'Введите свой вес'
                                        register = { form.register('weight') } />
                                </div>
                                <div className = { Styles.controls }>
                                    <button
                                        className = { Styles.clearData }
                                        onClick = { handleReset }
                                        disabled = {
                                            !form.formState.isDirty
                                        }>Сбросить</button>
                                    <button
                                        type = 'submit'
                                        disabled = {
                                            !form.formState.isDirty
                                        }>Обновить</button>
                                </div>
                                <button
                                    type = 'button'
                                    onClick = { handleClick }
                                    className = { Styles.clearAllRecords }>
                                    Очистить все данные
                                </button>

                            </form>

                        </div>
                    </div>
                </div>


            </div>


        </section>
    );
};

