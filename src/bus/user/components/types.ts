export interface ILoginFormShape {
    email: string;
    password: string;
}

export interface ISingUpFormShape {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}
