import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Input } from '../../../../elements/customInput/input';
import { useSingUp } from '../../../../hooks/useRegistration';
import { IProfile, Sex } from '../../../../types';
import { profileSchema } from './config';
import './styles/module.scss';

export const SingUp: React.FC = () => {
    const [sexInForm, setSexInForm] = useState('');
    const singUp = useSingUp();

    const form = useForm({
        mode:          'onTouched',
        defaultValues: {
            email:    '',
            fname:    '',
            lname:    '',
            password: '',
            age:      '',
            sex:      '',
            height:   '',
            weight:   '',
        },

        resolver: yupResolver(profileSchema),
    });

    const handleGender = (gender: Sex) => {
        form.setValue('sex', gender, { shouldDirty: true });
        setSexInForm(gender);
    };

    const handleReset = () => {
        form.setValue('sex', '', { shouldDirty: true });
        setSexInForm('');
        form.reset();
    };

    const submit = form.handleSubmit(async (values: IProfile) => {
        await singUp.mutateAsync(values);
    });


    return (
        <section className = 'registration'>
            <div className = 'left'>
                <div className = 'profile'>
                    <form onSubmit = { submit } >
                        <h1>Профиль</h1>
                        <div className = 'gender'>
                            <input
                                type = 'radio'
                                value = { Sex.FEMALE }
                                style = { { display: 'none' } }
                                { ...form.register('sex') } />
                            <div
                                className = { ` female ${sexInForm === Sex.FEMALE ? 'selected' : ''}  ` }
                                onClick = { () => handleGender(Sex.FEMALE) }>
                                <span>Женщина</span>
                            </div>

                            <input
                                type = 'radio'
                                value = { Sex.MALE }
                                style = { { display: 'none' } }
                                { ...form.register('sex') } />
                            <div
                                className = { ` male ${sexInForm === Sex.MALE ? 'selected' : ''}  ` }
                                onClick = { () => handleGender(Sex.MALE) }>
                                <span>Мужчина</span>
                            </div>
                        </div>
                        <div className = 'inputRow'>
                            <label>Электропочта</label>
                            <Input
                                type = 'email'
                                placeholder = 'Введите свою электропочту'
                                register = { form.register('email') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Имя</label>
                            <Input
                                type = 'text'
                                placeholder = 'Введите свое имя'
                                register = { form.register('fname') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Фамилия</label>
                            <Input
                                type = 'text'
                                placeholder = 'Введите свою фамилию'
                                register = { form.register('lname') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Пароль</label>
                            <Input
                                type = 'password'
                                placeholder = 'Введите свой пароль'
                                register = { form.register('password') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Возраст</label>
                            <Input
                                type = ''
                                placeholder = 'Введите свой возрост'
                                register = { form.register('age') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Рост</label>
                            <Input
                                type = ''
                                placeholder = 'Введите свой рост'
                                register = { form.register('height') } />
                        </div>
                        <div className = 'inputRow'>
                            <label>Вес</label>
                            <Input
                                type = ''
                                placeholder = 'Введите свой вес'
                                register = { form.register('weight') } />
                        </div>
                        <div className = 'controls'>
                            <button
                                className = 'clearData'
                                onClick = { handleReset }
                                disabled = {
                                    !form.formState.isDirty
                                }>Сбросить</button>
                            <button
                                type = 'submit'
                                disabled = {
                                    !form.formState.isDirty
                                }>Зарегистрироватся</button>
                        </div>
                    </form>

                </div>
            </div>
            <div className = 'right'></div>
        </section>
    );
};
