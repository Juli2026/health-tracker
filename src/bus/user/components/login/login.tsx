import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { NavLink } from 'react-router-dom';

import { Input } from '../../../../elements/customInput/input';
import { schema } from './config';
import './styles/module.scss';
import { useLogin } from '../../../../hooks/useLogin';
import { ILoginFormShape } from '../types';

export const Login: React.FC = () => {
    const login = useLogin();

    const form  = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ILoginFormShape) => {
        await login.mutateAsync(credentials);
        form.reset();
    });


    return (
        <section className = 'login'>
            <form onSubmit = { onSubmit } className = 'content'>
                <h1>Добро пожаловать!</h1>
                <div className = 'inputRow'>
                    <label>Электропочта</label>
                    <Input
                        placeholder = 'Введите свою электропочту'
                        error = { form.formState.errors.email }
                        register = { form.register('email') } />
                </div>
                <div className = 'inputRow'>
                    <label>Пароль</label>
                    <Input
                        type = 'password'
                        placeholder = 'Введите свой пароль'
                        error = { form.formState.errors.password }
                        register = { form.register('password') } />
                </div>
                <div>
                    <button type = 'submit'>Войти в систему</button>
                    <div className = 'loginLink'>
                        <p>Если у вас нет аккаунта, пожалуйста
                            <NavLink to = '/registration'> зарегистрируйтесь.</NavLink>
                        </p>
                    </div>
                </div>
            </form>

        </section>
    );
};

