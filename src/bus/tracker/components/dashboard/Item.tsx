import { FC } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import Styles from './styles/index.module.scss';

interface IItemProps {
    index: string,
    href: string;
    title: string;
    text: string;
}

export const Item: FC<IItemProps> = ({
    index, href, title, text,
}) => {
    return (
        <Link
            className = { cx(Styles.link, Styles[ `category${index}` ]) }
            to = { href }>
            <span className = { Styles.title }>{ title }</span>
            <span className = { Styles.description }>{ text }</span>
        </Link>
    );
};
