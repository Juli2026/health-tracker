import { navItems } from '../../../../constants/navLink';
import { Item } from './Item';
import Styles from './styles/index.module.scss';

export const DashboardItems: React.FC = () => {
    return (
        <div className = { Styles.dashboard }>
            <div className = { Styles.navigation }>
                <h1>Как у тебя проходит день?</h1>
                <div className = { Styles.items }>
                    { navItems.map((item) => <Item key = { item.index } { ...item } />) }
                </div>
            </div>
        </div>
    );
};
